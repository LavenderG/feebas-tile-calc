-- Adapted from Kaphotics' RNG for LUA
function rng(seed)
	return (0x4E6D * (seed % 65536) +((0x41C6 * (seed % 65536 )+ 0x4E6D * math.floor(seed / 65536)) % 65536) * 65536 +0x3039) % (4294967296)
end

-- Tux - https://www.smogon.com/forums/threads/past-gen-rng-research.61090/page-34#post-3986326
function is_feebas_tile(feebas_seed, current_tile)
	rng_seed = feebas_seed
	i = 0
	tile = 0
	while i <= 5 do
		rng_seed = rng(rng_seed)
		tile = (rng_seed >> 16) % 0x1BF
		if tile >= 4 then
			i = i + 1
		end
		if tile == current_tile then
			return true
		end
	end
	return false
end

function ask_seed_until_valid()
	io.write("Input Feebas seed value (number in range [0, 65535]:")
	input = io.read()
	input_digit = tonumber(input)
	while input_digit == nil or input_digit < 0 or input_digit > 65535 do
		-- Convert nil to a printable value
		if input == nil then
			input = "nil"
		end
		io.write("Invalid Feebas seed value: " .. input .. "\n")
		io.write("Please input a valid seed value (number in range [0, 65535]):")
		input = io.read()
		input_digit = tonumber(input)
	end

	return input_digit
end

-- Adapted from suloku - https://projectpokemon.org/home/forums/topic/37192-feebas-fishing-spot-value-rusaem/

feebas_seed =  ask_seed_until_valid()

io.write("\nFeebas seed: " .. feebas_seed .. "\n")

current_tile = 0

io.write ("Feebas tiles:\n")

while current_tile <= 600 do
	if is_feebas_tile(feebas_seed, current_tile) then
	 	io.write("\tTile: ".. current_tile .. "\n")
	 end
	 current_tile = current_tile + 1
end
