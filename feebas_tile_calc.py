import math
# Adapted from Kaphotics' RNG for LUA


def rng(seed):
    return (0x4E6D * (seed % 65536) + ((0x41C6 * (seed % 65536)
            + 0x4E6D * math.floor(seed / 65536))
        % 65536)
        * 65536 + 0x3039) % (4294967296)

# Tux
# https://www.smogon.com/forums/threads/past-gen-rng-research.61090/page-34#post-3986326


def is_feebas_tile(feebas_seed, current_tile):
    rng_seed = feebas_seed
    i = 0
    tile = 0
    while i <= 5:
        rng_seed = rng(rng_seed)
        tile = (rng_seed >> 16) % 0x1BF
        if tile >= 4:
            i = i + 1
        if tile == current_tile:
            return True
    return False


def ask_seed_until_valid():
    input_data = input("Input Feebas seed value (number in range [0, 65535]:")
    input_digit = None
    try:
        input_digit = int(input_data)
    except TypeError:
        input_digit = None
    while input_digit is None or input_digit < 0 or input_digit > 65535:
        print("Invalid Feebas seed value: {0}".format(input_data))
        input_data = input(
            "Please input a valid seed value (number in range [0, 65535]):")
        try:
            input_digit = int(input_data)
        except TypeError:
            input_digit = None

    return input_digit


# Adapted from suloku
# https://projectpokemon.org/home/forums/topic/37192-feebas-fishing-spot-value-rusaem/
feebas_seed = ask_seed_until_valid()

print("\nFeebas seed: {0}" .format(feebas_seed))

current_tile = 0

print("Feebas tiles:")

while current_tile <= 600:
    if is_feebas_tile(feebas_seed, current_tile):
        print("\tTile: {0}".format(current_tile))
    current_tile = current_tile + 1
