# feebas-tile-calc
Calculadora de casillas de Feebas para RZE.

## Uso

Ejecutar el script e introducir el número de Feebas actual de la partida.

Tras esto, el script indicará en qué casillas se puede atrapar a Feebas.
